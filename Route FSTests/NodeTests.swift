//
//  NodeTests.swift
//  Route FSTests
//
//  Created by Xavier McNulty on 5/7/19.
//  Copyright © 2019 Xavier McNulty. All rights reserved.
//

import XCTest

class NodeTests: XCTestCase {

    override func setUp() {
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }

    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
    }

    func testNodeBasic() {
        let bosApt = Node(ident: "KBOS", lat: 67.94830, lon: 56.3943, .APT)
        
        XCTAssert(bosApt.identifier == "KBOS")
        XCTAssert(bosApt.latitude == 67.94830)
        XCTAssert(bosApt.longitude == 56.3943)
        XCTAssert(bosApt.type == .APT)
        XCTAssert(bosApt.altitude.altitude == 0 && bosApt.altitude.unit == "ft")
        XCTAssert(bosApt.via.identifier == "" && bosApt.via.type == .DCT)
        XCTAssert(bosApt.name == "")
    }
    
    func testNodeWithAltDefaultunits() {
        let bosApt = Node(ident: "KBOS", lat: 67.94830, lon: 56.3943, .APT, Altitude(5000))
        
        XCTAssert(bosApt.identifier == "KBOS")
        XCTAssert(bosApt.latitude == 67.94830)
        XCTAssert(bosApt.longitude == 56.3943)
        XCTAssert(bosApt.type == .APT)
        XCTAssert(bosApt.altitude.altitude == 5000 && bosApt.altitude.unit == "ft")
        XCTAssert(bosApt.via.identifier == "" && bosApt.via.type == .DCT)
        XCTAssert(bosApt.name == "")
    }

    func testNodeWithAltMetric() {
        let bosApt = Node(ident: "KBOS", lat: 67.94830, lon: 56.3943, .APT, Altitude(5000, units: .METRIC))
        
        XCTAssert(bosApt.identifier == "KBOS")
        XCTAssert(bosApt.latitude == 67.94830)
        XCTAssert(bosApt.longitude == 56.3943)
        XCTAssert(bosApt.type == .APT)
        XCTAssert(bosApt.altitude.altitude == 5000 && bosApt.altitude.unit == "m")
        XCTAssert(bosApt.via.identifier == "" && bosApt.via.type == .DCT)
        XCTAssert(bosApt.name == "")
    }
    
    func testNodeWithAltSI() {
        let bosApt = Node(ident: "KBOS", lat: 67.94830, lon: 56.3943, .APT, Altitude(5000, units: .SI))
        
        XCTAssert(bosApt.identifier == "KBOS")
        XCTAssert(bosApt.latitude == 67.94830)
        XCTAssert(bosApt.longitude == 56.3943)
        XCTAssert(bosApt.type == .APT)
        XCTAssert(bosApt.altitude.altitude == 5000 && bosApt.altitude.unit == "m")
        XCTAssert(bosApt.via.identifier == "" && bosApt.via.type == .DCT)
        XCTAssert(bosApt.name == "")
    }
    
    func testNodeWithAltAviation() {
        let bosApt = Node(ident: "KBOS", lat: 67.94830, lon: 56.3943, .APT, Altitude(5000, units: .AVIATION))
        
        XCTAssert(bosApt.identifier == "KBOS")
        XCTAssert(bosApt.latitude == 67.94830)
        XCTAssert(bosApt.longitude == 56.3943)
        XCTAssert(bosApt.type == .APT)
        XCTAssert(bosApt.altitude.altitude == 5000 && bosApt.altitude.unit == "ft")
        XCTAssert(bosApt.via.identifier == "" && bosApt.via.type == .DCT)
        XCTAssert(bosApt.name == "")
    }
}
