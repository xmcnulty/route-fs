//
//  APIHandlerTests.swift
//  Route FSTests
//
//  Created by Xavier McNulty on 5/8/19.
//  Copyright © 2019 Xavier McNulty. All rights reserved.
//

import XCTest

class APIHandlerTests: XCTestCase {

    override func setUp() {
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }

    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
    }

    func testAPIFetchGood() {
        // This is an example of a functional test case.
        // Use XCTAssert and related functions to verify your tests produce the correct results.
        let handler = APIHandler()
        let exp = self.expectation(description: "Fetch call back is called.")
    
        var fp:FlightPlan? = nil
        
        // Testing against a pre-existing route KSAN BROWS TRM LRAIN KDEN
        handler.fetch(id: 62478, completionHandler: {
            plan in
            
            if plan != nil {
                fp = plan
                exp.fulfill()
            } else {
                XCTAssertTrue(false)
            }
            
        })
    
        waitForExpectations(timeout: 10, handler: { error in
        
        })
        
        // Check Valid Airports
        XCTAssertTrue(fp!.fromICAO == "KSAN")
        XCTAssertTrue(fp!.toICAO == "KDEN")
        XCTAssertTrue(fp!.fromName == "San Diego Intl")
        XCTAssertTrue(fp!.toName == "Denver Intl")
        
        // Check waypoints
        let route = fp!.route
        
        XCTAssertTrue(route.count == 5)
        
        XCTAssertTrue(route[0].identifier == "KSAN")
        XCTAssertTrue(route[0].type == .APT)
        
        XCTAssertTrue(route[1].identifier == "BROWS")
        XCTAssertTrue(route[1].type == .FIX)
        
        XCTAssertTrue(route[4].identifier == "KDEN")
        XCTAssertTrue(route[4].type == .APT)
    }

    // Testing the fetch of an invalid flightplan id.
    func testAPIFetchInvalidId() {
        let handler = APIHandler()
        let exp = self.expectation(description: "Fetch call back is called.")
        
        var fp:FlightPlan? = nil
        
        // Testing against a pre-existing route KSAN BROWS TRM LRAIN KDEN
        handler.fetch(id: 0, completionHandler: {
            plan in
            
            if plan != nil {
                fp = plan
                exp.fulfill()
            } else {
                XCTAssertTrue(false)
            }
            
        })
    
        waitForExpectations(timeout: 20, handler: { error in
        
        })
        
        XCTAssertNil(fp)
    }
    
    func testPerformanceExample() {
        // This is an example of a performance test case.
        self.measure {
            // Put the code you want to measure the time of here.
        }
    }

}
