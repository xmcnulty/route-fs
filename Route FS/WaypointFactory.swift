//
//  WaypointFactory.swift
//  Route FS
//
//  Created by Xavier McNulty on 5/7/19.
//  Copyright © 2019 Xavier McNulty. All rights reserved.
//

import Foundation

/// Valid types of Nodes, as defined in the Flight Plan Database (FPD) API.
/// These are Airport, VHF omni-directional range (VOR), Non-directional beacon (NDB),
/// distance measuring equipment, latitude/longitude pint, navigational fix, and unknown.
///
/// - Author: Xavier McNulty
/// - Date: 7 May 2019
enum NodeType {
    case Airport
    case VOR
    case NDB
    case DME
    case LatLon
    case Fix
    case Unknown
}

class Via {
    enum ViaType {
        case SID // Standard instrument departure
        case STAR // Standard arrival procedure
        case AWY_HI // High airway
        case AWY_LO // Low airway
        case NAT // North Atlantic Track
        case PACOT // Pacific Track
        case DCT // Direct
    }
    
    let type: ViaType
    let identifier: String
    
    init(type: ViaType = .DCT, ident: String = "DCT") {
        self.type = type
        identifier = ident
    }
}

class Altitude {
    enum AltUnit {
        case aviation
        case si
        case metric
    }
    
    let unit: AltUnit
    let altitude: Int
    
    init(alt: Int = 0, aUnit: AltUnit = .aviation) {
        altitude = alt
        unit = aUnit
    }
}

/**
 A node (waypoint) in a route as definied in the FPD API. Every route as an airnav idenfier, latitude in decimal degrees,
 longitude in decimal degrees, a type (airport, VOR, DME, NDB, LatLon, Fix, Unknown), a suggested altitude,
 a name (optional), and a via (route to node. ex. airway, SID, STAR...) (optional).
 
 - Author: Xavier McNulty
 - Date: 7 May 2019
 */
class Node {
    let identifier: String
    let name: String
    let latitude, longitude: Decimal
    let type: NodeType
    let via: Via // Route to node from the previos (ex. airway). Optional.
    let altitude: Altitude // suggested altitude
    
    /**
     Creates a new Node
    */
    init (ident: String, lat: Decimal, lon: Decimal, _ type: NodeType, _ name: String? = nil, _ via: Via? = nil, _ alt: Altitude? = nil) {
        identifier = ident
        latitude = lat
        longitude = lon
        self.type = type
        self.name = name == nil ? "" : name!
        self.via = via == nil ? Via() : via!
        altitude = alt == nil ? Altitude() : alt!
    }
}
