//
//  FlightPlan.swift
//  Route FS
//
//  Created by Xavier McNulty on 5/8/19.
//  Copyright © 2019 Xavier McNulty. All rights reserved.
//

import Foundation

/**
 Class representaiton of a flight plan as specified in the Flight Plan Database API.
 A flight plan is contained in a JSON response from the API. This class contains the methods that parse the responses
 from the API. FlightPlans can only be created from a handful of factory methods.
 
 - Author: Xavier McNulty
 */
class FlightPlan {
    let id: Int // Unique id of the plan from Flight Plan Database
    let route: [Node] // Array of ordered Nodes that make up the route
    let fromICAO: String, toICAO: String
    let fromName: String, toName: String
    let flightNumber: String
    let maxAltitude: Altitude
    let distance: Double
    let numWaypoints: Int
    let notes: String
    let encodedPolyLine: String // for plotting on a map
 
    private init(id: Int,
         route: [Node],
         distance: Double,
         numWaypoints: Int,
         maxAltitude: Altitude,
         fromICAO: String?,
         toICAO: String?,
         fromName: String?,
         toName: String?,
         flightNum: String?,
         notes: String?,
         encodedPolyLine: String?) {
        
        
        self.id = id
        self.route = route
        self.distance = distance
        self.numWaypoints = numWaypoints
        self.maxAltitude = maxAltitude
        self.fromICAO = fromICAO == nil ? "" : fromICAO!
        self.toICAO = toICAO == nil ? "" : toICAO!
        self.fromName = fromName == nil ? "" : fromName!
        self.toName = toName == nil ? "" : toName!
        flightNumber = flightNum == nil ? "" : flightNum!
        self.notes = notes == nil ? "" : notes!
        self.encodedPolyLine = encodedPolyLine == nil ? "" : encodedPolyLine!
    }
    
    /**
     Creates a FlightPlan from a dictionary representation of a FlightPlan, typically created from the JSON response from the API.
     
     - Parameters:
         - dictionary: FlightPlan fields where the keys (String) are the field names and the values (Any) are their values.
         - units: Unit type returned by the API
     
     - Returns: FlightPlan that is parsed from the given dictionary.
    */
    static func createFromDictionary(_ dictionary: [String : Any], units: Units) -> FlightPlan? {
        var flightPlan: FlightPlan? = nil
        
        if let route = dictionary["route"] as? [String: [[String: Any]]] {
            let id = dictionary["id"] as? Int
            let fromICAO = dictionary["fromICAO"] as? String
            let toICAO = dictionary["toICAO"] as? String
            let fromName = dictionary["fromName"] as? String
            let toName = dictionary["toName"] as? String
            let flightNumber = dictionary["flightNumber"] as? String
            let distance = dictionary["distance"] as? Double
            let maxAltitude = dictionary["maxAltitude"] as? Int
            let numWaypoints = dictionary["waypoints"] as? Int
            let notes = dictionary["notes"] as? String
            let encodedPolyline = dictionary["encodedPolyline"] as? String
            
            let maxAlt = maxAltitude != nil ? Altitude(maxAltitude!, units: units) : Altitude(units: units)
            
            if id != nil && distance != nil && numWaypoints != nil, let nodes = parseRoute(route["nodes"], units) {
                
                flightPlan = FlightPlan(id: id!, route: nodes, distance: distance!, numWaypoints: numWaypoints!, maxAltitude: maxAlt, fromICAO: fromICAO, toICAO: toICAO, fromName: fromName, toName: toName, flightNum: flightNumber, notes: notes, encodedPolyLine: encodedPolyline)
            }
        }
        
        return flightPlan
    }
    
    /**
     Creates a FlightPlan from JSON data returned from an API call.
     
     - Parameters:
        - data: Raw HTTPURLResponse data returned from a call to the Flight Plan Database API.
        - units: Unit type returned by the API
     
     - Returns: FlightPlan created from data.
    */
    static func createFromJSONData(data: Data?, units: Units) -> FlightPlan? {
        if data == nil {
            return nil
        }
        
        if let dictionary = try? JSONSerialization.jsonObject(with: data!, options: []) as? [String: Any] {
            return createFromDictionary(dictionary, units: units)
        } else {
            return nil
        }
    }
    
    /**
     Parses a route object (array of nodes) from an the jsonObject of the API response to an Array of Node objects.
     
     - Parameters:
         - nodes: Array of dictionaries, each dictionary holds all the fields of a Node.
         - units: Unit type returned by the API
     
     - Returns: Array of Nodes or nil if an error occured
    */
    private static func parseRoute(_ nodes: [[String : Any]]?, _ units: Units) -> [Node]? {
        var route: [Node] = []
        
        if nodes == nil {
            return nil
        }
        
        for node in nodes! {
            let type = node["type"] as? String
            let ident = node["ident"] as? String
            let name = node["name"] as? String
            let lat = node["lat"] as? Double
            let lon = node["lon"] as? Double
            
            // set altitude
            let alt = node["alt"] as? Int
            let altitude = alt == nil ? Altitude() : Altitude(alt!, units: units)
            
            // set via
            var via: Via? = nil
            if let viaObj = node["via"] as? [String : Any] {// via object returned by api, or nil
                via = Via(type: viaObj["type"]! as! Via.ViaType, ident: viaObj["ident"]! as! String)
            }
            
            
            if ident == nil || lat == nil || lon == nil || type == nil {
                return nil
            }
            
            route.append(Node(ident: ident!, lat: lat!, lon: lon!, NodeType(rawValue: type!)!, altitude, name, via))
        }
        
        return route
    }
}
