//
//  WaypointFactory.swift
//  Route FS
//
//  Created by Xavier McNulty on 5/7/19.
//  Copyright © 2019 Xavier McNulty. All rights reserved.
//

import Foundation

/// Valid types of Nodes, as defined in the Flight Plan Database (FPD) API.
/// These are Airport, VHF omni-directional range (VOR), Non-directional beacon (NDB),
/// distance measuring equipment, latitude/longitude pint, navigational fix, and unknown.
///
/// - Author: Xavier McNulty
/// - Date: 7 May 2019
enum NodeType: String {
    case APT, VOR, NDB, DME, LATLON, FIX, UKN
}

/**
 A route between two nodes in a flight plan. Valid types are standard instrument departure (SID), standard terminal arrival (STAR),
 high airway, low airway, North Atlantic Track (NAT), Pacific Organized Track (PACOT), Direct (default).
 
 - Author: Xavier McNulty
 - Date: 7 May 2019
 */
class Via {
    enum ViaType: String {
        case SID // Standard instrument departure
        case STAR // Standard terminal arrival
        case AWY_HI // High airway
        case AWY_LO // Low airway
        case NAT // North Atlantic Track
        case PACOT // Pacific Organized Track
        case DCT = "" // Direct
    }
    
    let type: ViaType
    let identifier: String
    
    init(type: ViaType = .DCT, ident: String = ViaType.DCT.rawValue) {
        self.type = type
        identifier = ident
    }
}

/** Valid units (aviation, SI, metric) as specified in the FDB API.
 
 
 Number Type               |  AVIATION         |  METRIC             |  SI
 ----------------------------------------------------------------------------------------
 Length                    |  Feet             |  Meters             |  Meters
 Distance                  |  Nautical Miles   |  Kilometers         |  Meters
 Elevation                 |  Feet             |  Meters             |  Meters
 Altitude                  |  Feet             |  Meters             |  Meters
 Speed                     |  Knots            |  Meters per Second  |  Meters per Second
 Temperature               |  Celsius          |  Celsius            |  Kelvin
 Climb (and Descent) Rate  |  Feet per Minute  |  Meters per Second  |  Meters per Second
 
 */
enum Units: String {
    case AVIATION, METRIC, SI
}

/// Altitude with units (aviation, SI, metric) for use with a node. Defaults to 0m.
/// - Author: Xavier McNulty
/// - Date: 7 May 2019
class Altitude {
    
    let unit: String // Either feet 'ft' or meters 'm'
    let altitude: Int
    
    /**
    Creates a new altitude with units. Defaults to 0 meters (metric).
 
     - Parameter alt: Whole integer altitude value.
     - Parameter unit: Altitude unit (aviation, SI, metric)
     
     - Returns: Altitude with unit
    */
    init(_ alt: Int = 0, units: Units = .AVIATION) {
        altitude = alt
        
        switch units {
        case .AVIATION:
            self.unit = "ft"
        default:
            self.unit = "m"
        }
    }
}

/**
 A node (waypoint) in a route as definied in the FPD API. Every route as an airnav idenfier, latitude in decimal degrees,
 longitude in decimal degrees, a type (airport, VOR, DME, NDB, LatLon, Fix, Unknown), a suggested altitude,
 a name (optional), and a via (route to node. ex. airway, SID, STAR...) (optional).
 
 - Author: Xavier McNulty
 - Date: 7 May 2019
 */
class Node {
    let identifier: String
    let name: String
    let latitude: Double, longitude: Double
    let type: NodeType
    let via: Via // Route to node from the previos (ex. airway). Optional.
    let altitude: Altitude // suggested altitude
    
    /**
     Creates a new Node with the attributes defined in the FPD API.
     
     - Parameters:
        - ident: Airnav idenifier
        - lat: Latitude in decimal degrees
        - lon: Longitude in decimal degrees
        - type: Node type (airport, VOR, DME, NDB, LatLon, Fix, Unknown)
        - name: Node name (optional)
        - via: Route to node from previous node (ex. airway) (optional)
        - alt: Suggested altitude at node. With units. (optional)
     
     - Returns: A new immutable Node with the given attributes
    */
    init (ident: String, lat: Double, lon: Double, _ type: NodeType, _ alt: Altitude? = nil, _ name: String? = nil, _ via: Via? = nil) {
        identifier = ident
        latitude = lat
        longitude = lon
        self.type = type
        self.name = name == nil ? "" : name!
        self.via = via == nil ? Via() : via!
        altitude = alt == nil ? Altitude() : alt!
    }
}
