//
//  APIHandler.swift
//  Route FS
//
//  Created by Xavier McNulty on 5/8/19.
//  Copyright © 2019 Xavier McNulty. All rights reserved.
//

import Foundation

enum HTTPMethod: String {
    case GET, POST, PATCH, DELETE
}

/**
 Class that handles HTTPSURLRequest calls to the Flight Plan Database API.
 
 - Author: Xavier McNulty
 */
class APIHandler {
    
    private let authorization = "Basic lvkSTwD6rpXdkxgnGqFkdPE4s1yzRQ6YXKSwz6cY"
    private let accept = "application/json"
    private let fetchUrl = "https://api.flightplandatabase.com/plan/"
    
    private let session: URLSession
    
    init() {
        session = URLSession.shared
    }
    
    /**
     Sends a request to the server that requests a flight plan by id. The respones from the server is processed in the completion handler.
     
     - Parameters:
         - id: ID of the flight plan being fetched
         - units: Requested units for flight plan distances and altitudes. See API documentation for valid Unit types
         - completionHandler: Function that passes FlightPlan optional
    */
    func fetch(id: Int, units: Units = .AVIATION, completionHandler: @escaping (FlightPlan?) -> ()) {
        let url = "\(fetchUrl)\(id)"
        
        sendRequest(url: url, method: .GET, units: units, completionHandler: {
            data, httpResponse in
            
            var flightPlan: FlightPlan? = nil
            
            let respUnits: Units
            
            if let httpResponse = httpResponse, let unit = httpResponse.allHeaderFields["x-units"] as? String {
                respUnits = Units(rawValue: unit)!
            } else {
                respUnits = .AVIATION
            }
            
            flightPlan = FlightPlan.createFromJSONData(data: data, units: respUnits)
            
            return completionHandler(flightPlan)
        })
    }
    
    /**
     Sends an HTTP request to the server. The request can either be bodiless or contain data that can be put into a JSON object.
     Response processing depends on the request being sent. API documentation should be consulted for proper requests, and their responses.
     
     - Parameters:
         - url: URL String that is the recipient of the request
         - method: HTTP Method of the call
         - units: Value of the units header (optional). Defaults to aviation units. See the API for proper values
         - completionHandler: Function that process the response from the server. This function takes the response data (Data?)and HTTP response body (HTTPURLResponse?)
     */
    private func sendRequest(url: String , method: HTTPMethod, units: Units = .AVIATION, completionHandler: @escaping (Data?, HTTPURLResponse?) -> ()) {
        var request = URLRequest(url: URL(string: url)!)
        
        request.httpMethod = method.rawValue
        request.addValue("Authorization", forHTTPHeaderField: authorization)
        request.addValue("Accept", forHTTPHeaderField: accept)
        request.addValue("X-Units", forHTTPHeaderField: units.rawValue)
        
        session.dataTask(with: request, completionHandler: {
            data, response, error in
            
            if error == nil, let httpResponse = response as? HTTPURLResponse, httpResponse.statusCode == 200 {
                
                return completionHandler(data, httpResponse)
            } else {
                return completionHandler(nil, response as? HTTPURLResponse)
            }
        }).resume()
    }
}
